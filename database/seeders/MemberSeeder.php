<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $firstname = ["OBIDJON","AZIZBEK","RUSTAM","ABROR","TO'LQIN","YORQIN","ZIYODULLA","AKRAM","BAXTIYOR"];
        $lastname = ["XAMIDOV","AMONOV","JUMAYEV","JURAYEV","RASULOV","QULIYEV","NUROV","QILICHEV","QOBILOV"];
        $surname = ["XAFIZOVICH","AMONJONOVICH","G'ANIYEVICH","TUROBOVICH","HUSENOVICH","KARIMOVICH","SAYMURODOVICH","ABDUSAMADOVICH","NMADIR"];
        $users = [14396, 46790, 13627, 5050, 13047, 12542, 13009, 13539, 00000];
        $fios = ["O.X.Xamidov","A.A.Amonov", "R.G‘.Jumayev", "A.T.Jo‘rayev", "T.H.Rasulov", "Y.K.Quliyev", "Z.S.Nurov", "A.A.Qilichev", "B.B.Qobilov"];

        for ($i = 0; $i < count($firstname); $i++) {
            DB::table('members')
                ->insert([
                    'user_id' => $users[$i],
                    'firstname' => $firstname[$i],
                    'lastname' => $lastname[$i],
                    'surname' => $surname[$i],
                ]);
        }
    }
}
