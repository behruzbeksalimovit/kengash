<?php

namespace Database\Seeders;

use App\Domain\Secretaries\Members\Models\Member;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DirectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $titles = [
            "Universitet jamoasi bilan amalga oshiriladigan masalalar yo‘nalishi",
            "Yoshlar masalalari va ma’naviy-ma’rifiy ishlar yo‘nalishi",
            "O‘quv jarayoni yo‘nalishi",
            "Xalqaro hamkorlik yo‘nalishi",
            "Ilmiy ishlar va innovatsiyalar yo‘nalishi",
            "Korrupsiyaga qarshi kurashish “Komplaens nazorat” tizimini yo‘nalishi",
            "Ta’lim sifatini nazorat qilish",
            "Murojaatlar bilan ishlash, nazorat va ish yuritish departamenti yo‘nalishi",
            "Universitet kasaba uyushmasi yo‘nalishi",
        ];

        $users = [14396, 46790, 13627, 5050, 13047, 12542, 13009, 13539, 0];
        $fios = ["O.X.Xamidov","A.A.Amonov", "R.G'.Jumayev", "A.T.Jo‘rayev", "T.H.Rasulov", "Y.K.Quliyev", "Z.S.Nurov", "A.A.Qilichev", "B.B.Qobilov"];

        for ($i = 0; $i < count($titles); $i++) {
            DB::table('directions')
                ->insert([
                    'user_id' =>$users[$i],
                    'title' => $titles[$i]
                ]);
        }

    }
}

