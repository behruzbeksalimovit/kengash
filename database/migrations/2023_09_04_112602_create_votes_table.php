<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('agenda_id')->index();
            $table->unsignedBigInteger('issue_id')->index()->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->integer('status');
            $table->integer('disabled')->nullable();
            $table->timestamps();

            $table->foreign('agenda_id')->references('id')->on('agendas');
            $table->foreign('issue_id')->references('id')->on('issues');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('votes');
    }
};
