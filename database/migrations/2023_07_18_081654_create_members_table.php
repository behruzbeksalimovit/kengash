<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->comment('uniworkdagi foydalanuvchining id si');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('surname');
            $table->boolean('status')->default(true)->comment('azolarni o`chirib qoyish uchun yoki yoqish uchun');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('members');
    }
};
