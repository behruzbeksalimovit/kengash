<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('responsible_id');
            $table->unsignedBigInteger('reporter_id');
            $table->text('title')->comment('Muhokama etiladigan masalalar');
            $table->string('responsible')->comment('Qaror loyihasini tayyorlash bo‘yicha mas’ullar');
            $table->string('lifetime')->comment('Qaror loyihasini tayyorlash muddati');
            $table->string('reporter')->comment('hisobot beruvchi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plans');
    }
};
