<?php

namespace App\Http\Middleware;

use App\Domain\Secretaries\Members\Models\Member;
use App\Domain\Traits\LoginTrait;
use Closure;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Response;

class MemberMiddleware
{
    use LoginTrait;

    /**
     * @param Request $request
     * @param Closure $next
     * @return Response
     * @throws GuzzleException
     */
    public function handle(Request $request, Closure $next): Response
    {
        $result = $this->login($request);

        $members = Member::query()
            ->select('user_id', 'status','active')
            ->get();

        if (isset($result->code) == false
//            && (
//                $result->id == env('A_A_AMONOV') ||
//                $result->id == env('A_A_QILICHEV') ||
//                $result->id == env('A_T_JURAYEV') ||
//                $result->id == env('B_B_QOBILOV') ||
//                $result->id == env('Z_S_NUROV') ||
//                $result->id == env('Y_K_QULIYEV') ||
//                $result->id == env('T_H_RASULOV') ||
//                $result->id == env('R_G_JUMAYEV') ||
//                $result->id == env('MEMBER_TEST')
//            )
        ) {
            if (count($members->whereIn('user_id', [$result->id])) != 0) {
                foreach ($members as $member) {
                    if ($member->status != 0) {
                        if ($result->id == $member->user_id) {
                            if($member->active != 0){
                                return $next($request);
                            }else{
                                return response()
                                    ->json([
                                        'status' => false,
                                        'message' => 'Siz sahifaga kirmadingiz hali.'
                                    ]);
                            }
                        }
                    } else {
                        return response()
                            ->json([
                                'status' => false,
                                'message' => 'Siz a`zolikdan chiqarildingiz.'
                            ]);
                    }
                }
            } else {
                return response()
                    ->json([
                        'status' => false,
                        'message' => 'Bunday foydalanuvchi malumotlar bazasida mavjud emas.'
                    ]);
            }
        } else {
            return response()
                ->json([
                    'status' => false,
                    'message' => isset($result->error->message) ? $result->error->message : 'Bunday foydalanuvchi mavjud emas.'
                ]);
        }
    }
}
