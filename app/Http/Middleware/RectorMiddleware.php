<?php

namespace App\Http\Middleware;

use App\Domain\Traits\LoginTrait;
use Closure;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RectorMiddleware
{
    use LoginTrait;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     * @return Response
     * @throws GuzzleException
     */
    public function handle(Request $request, Closure $next): Response
    {
        $result = $this->login($request);

        if (isset($result->code) == false && ($result->id == env('RECTOR') || $result->id == env('RECTOR_TEST'))) {
            return $next($request);
        } else {
            return response()
                ->json([
                    'status' => false,
                    'message' => isset($result->error->message) ? $result->error->message : 'Bunday foydalanuvchi mavjud emas.'
                ]);
        }
    }
}
