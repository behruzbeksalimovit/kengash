<?php

namespace App\Http\Controllers\Departments;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function departments(): JsonResponse
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://uniwork.buxdu.uz/api/departments.asp');

        $departments = json_decode($response->getBody())->departments;

        return response()
            ->json([
                'status' => true,
                'data' => $departments
            ]);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function departmentSub($id): JsonResponse
    {
        $client = new \GuzzleHttp\Client();
        $response_sub_id = $client->request('POST', 'https://uniwork.buxdu.uz/api/departments_filter.asp?sub=' . $id);
        $departments = json_decode($response_sub_id->getBody())->departments;

        return response()
            ->json([
                'status' => true,
                'data' => $departments
            ]);
    }
}
