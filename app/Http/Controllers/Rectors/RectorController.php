<?php

namespace App\Http\Controllers\Rectors;

use App\Events\AttendanceEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RectorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($user_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = [
            "Access-Control-Allow-Origin" => "https://uniwork.buxdu.uz"
        ];
        $response = $client->get( 'https://uniwork.buxdu.uz/img_humens/'.$user_id.'.jpg',[
            'headers' => $headers
        ]);
        $imageContents = $response->getBody()->getContents();
        // Return the image with the appropriate content type
        return  response($imageContents);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function attendance($attendance)
    {
//        event(new AttendanceEvent($attendance));
    }
}
