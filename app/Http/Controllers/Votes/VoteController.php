<?php

namespace App\Http\Controllers\Votes;

use App\Domain\Votes\Actions\StoreVoteAction;
use App\Domain\Votes\DTO\StoreVoteDTO;
use App\Domain\Votes\Models\Vote;
use App\Domain\Votes\Repositories\VoteRepository;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class VoteController extends Controller
{
    /**
     * @var VoteRepository
     */
    public $votes;

    /**
     * @param VoteRepository $voteRepository
     */
    public function __construct(VoteRepository $voteRepository)
    {
        $this->votes = $voteRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->votes->getPaginate()
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->votes->getAll()
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function tarafdorlar($agenda_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->tarafdorlar($agenda_id)
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function betaraflar($agenda_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->betaraflar($agenda_id)
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function qarshilar($agenda_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->qarshilar($agenda_id)
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function ovozbermaganlar($agenda_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->ovozbermaganlar($agenda_id)
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function jami($agenda_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->jami($agenda_id)
            ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, StoreVoteAction $action): JsonResponse
    {
        try {
            $request->validate([
                'agenda_id' => 'required',
                'user_id' => 'required',
                'status' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreVoteDTO::fromArray($request->all());
            $response = $action->execute($dto);
            if ($response != null) {
                return response()->json([
                    'status' => true,
                    'message' => 'voted successfully.',
                    'data' => $response
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Siz birmarta ovoz berishingiz kerak.'
                ]);
            }

        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function filtrIssue($issue_id)
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->votes->filtrIssue($issue_id)
            ]);
    }

    public function getIssueVoteTarafdorlar($issue_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->getVoteIssueTarafdorlar($issue_id)
            ]);
    }

    public function getIssueVoteBetaraflar($issue_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->getVoteIssueBetaraflar($issue_id)
            ]);
    }

    public function getIssueVoteQarshilar($issue_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->getVoteIssueQarshilar($issue_id)
            ]);
    }

    public function getIssueVoteOvozBermaganlar($issue_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->getVoteIssueOvozBermaganlar($issue_id)
            ]);
    }

    public function getIssueVoteJami($issue_id)
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->votes->getVoteIssueJami($issue_id)
            ]);
    }
}
