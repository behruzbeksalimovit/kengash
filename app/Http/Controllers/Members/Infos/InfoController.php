<?php

namespace App\Http\Controllers\Members\Infos;

use App\Domain\Members\Infos\Actions\StoreInfoAction;
use App\Domain\Members\Infos\Actions\UpdateInfoAction;
use App\Domain\Members\Infos\DTO\StoreInfoDTO;
use App\Domain\Members\Infos\DTO\UpdateInfoDTO;
use App\Domain\Members\Infos\Models\Info;
use App\Domain\Members\Infos\Repositories\InfoRepository;
use App\Http\Controllers\Controller;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class InfoController extends Controller
{
    /**
     * @var InfoRepository
     */
    public $infos;

    /**
     * @param InfoRepository $infoRepository
     */
    public function __construct(InfoRepository $infoRepository)
    {
        $this->infos = $infoRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function index(Request $request): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->infos->getPaginate($request)
            ]);
    }

    /**
     * @param Request $request
     * @param StoreInfoAction $action
     * @return JsonResponse
     */
    public function store(Request $request, StoreInfoAction $action): JsonResponse
    {
        try {
            $request->validate([
                'issue_id' => 'required',
                'title' => 'required',
                'description' => 'required',
                'decision' => 'required',
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreInfoDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data created successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * @param Request $request
     * @param Info $info
     * @param UpdateInfoAction $action
     * @return JsonResponse
     */
    public function update(Request $request, Info $info, UpdateInfoAction $action): JsonResponse
    {
        try {
            $request->validate([
                'issue_id' => 'required',
                'title' => 'required',
                'description' => 'required',
                'decision' => 'required',
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $request->merge([
                'info' => $info
            ]);
            $dto = UpdateInfoDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data updated successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Info $info): JsonResponse
    {
        $info->delete();
        return response()->json([
            'status' => true,
            'message' => 'data deleted successfully.'
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->infos->getAll()
        ]);
    }

    /**
     * @param $issue_id
     * @return JsonResponse
     */
    public function getMemberAgenda($issue_id): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->infos->getMemberAgenda($issue_id)
        ]);
    }

    /**
     * @param $issue_id
     * @param $user_id
     * @return JsonResponse
     */
    public function getMemberIssueRectorAll($issue_id,$user_id): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->infos->getMemberIssueRectorAll($issue_id,$user_id)
        ]);
    }

    /**
     * @param $issue_id
     * @return JsonResponse
     */
    public function getIssueInfo($issue_id): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->infos->getIssueInfo($issue_id)
        ]);
    }
}
