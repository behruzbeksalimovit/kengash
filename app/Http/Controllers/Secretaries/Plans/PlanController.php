<?php

namespace App\Http\Controllers\Secretaries\Plans;

use App\Domain\Secretaries\Plans\Actions\StorePlanAction;
use App\Domain\Secretaries\Plans\Actions\UpdatePlanAction;
use App\Domain\Secretaries\Plans\DTO\StorePlanDTO;
use App\Domain\Secretaries\Plans\DTO\UpdatePlanDTO;
use App\Domain\Secretaries\Plans\Models\Plan;
use App\Domain\Secretaries\Plans\Repositories\PlanRepository;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PlanController extends Controller
{
    /**
     * @var PlanRepository
     */
    public $plans;

    /**
     * @param PlanRepository $planRepository
     */
    public function __construct(PlanRepository $planRepository)
    {
        $this->plans = $planRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->plans->getPaginate()
            ]);
    }

    /**
     * @param Request $request
     * @param StorePlanAction $action
     * @return JsonResponse
     */
    public function store(Request $request, StorePlanAction $action): JsonResponse
    {
        try {
            $request->validate([
                'responsible_id' => 'required',
                'reporter_id' => 'required',
                'title' => 'required',
                'responsible' => 'required',
                'reporter' => 'required',
                'lifetime' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StorePlanDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data created successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * @param Request $request
     * @param Plan $plan
     * @param UpdatePlanAction $action
     * @return JsonResponse
     */
    public function update(Request $request, Plan $plan, UpdatePlanAction $action): JsonResponse
    {
        try {
            $request->validate([
                'responsible_id' => 'required',
                'reporter_id' => 'required',
                'title' => 'required',
                'responsible' => 'required',
                'reporter' => 'required',
                'lifetime' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $request->merge([
                'plan' => $plan
            ]);
            $dto = UpdatePlanDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data updated successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * @param Plan $plan
     * @return JsonResponse
     */
    public function destroy(Plan $plan): JsonResponse
    {
        $plan->delete();
        return response()->json([
            'status' => true,
            'message' => 'data deleted successfully'
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->plans->getAll()
        ]);
    }
}
