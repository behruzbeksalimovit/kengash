<?php

namespace App\Http\Controllers\Secretaries\Agendas;

use App\Domain\Secretaries\Agendas\Actions\StoreAgendaAction;
use App\Domain\Secretaries\Agendas\Actions\UpdateAgendaAction;
use App\Domain\Secretaries\Agendas\DTO\StoreAgendaDTO;
use App\Domain\Secretaries\Agendas\DTO\UpdateAgendaDTO;
use App\Domain\Secretaries\Agendas\Models\Agenda;
use App\Domain\Secretaries\Agendas\Repositories\AgendaRepository;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AgendaController extends Controller
{
    /**
     * @var AgendaRepository
     */
    public $agendas;

    /**
     * @param AgendaRepository $agendaRepository
     */
    public function __construct(AgendaRepository $agendaRepository)
    {
        $this->agendas = $agendaRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->agendas->getPaginate()
            ]);
    }

    /**
     * @param Request $request
     * @param StoreAgendaAction $action
     * @return JsonResponse
     */
    public function store(Request $request, StoreAgendaAction $action): JsonResponse
    {
        try {
            $request->validate([
                'title' => 'required',
                'lifetime' => 'required',
                'input_time' => 'required',
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreAgendaDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data created successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * @param Request $request
     * @param Agenda $agenda
     * @param UpdateAgendaAction $action
     * @return JsonResponse
     */
    public function update(Request $request, Agenda $agenda, UpdateAgendaAction $action): JsonResponse
    {
        try {
            $request->validate([
                'title' => 'required',
                'lifetime' => 'required',
                'input_time' => 'required',
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $request->merge([
                'agenda' => $agenda
            ]);
            $dto = UpdateAgendaDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data updated successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * @param Agenda $agenda
     * @return JsonResponse
     */
    public function destroy(Agenda $agenda): JsonResponse
    {
        $agenda->delete();

        return response()->json([
            'status' => true,
            'message' => 'data deleted successfully.'
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->agendas->getAll()
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function getPaginateHomePage(): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->agendas->getPaginateHomePage(),
        ]);
    }




//    RECTOR PAGE
    public function getRectorAgenda()
    {
        return response()->json([
            'status' => true,
            'data' => $this->agendas->getPaginate(),
        ]);
    }

}
