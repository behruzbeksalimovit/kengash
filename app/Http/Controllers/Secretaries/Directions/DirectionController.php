<?php

namespace App\Http\Controllers\Secretaries\Directions;

use App\Domain\Secretaries\Directions\Actions\StoreDirectionAction;
use App\Domain\Secretaries\Directions\Actions\UpdateDirectionAction;
use App\Domain\Secretaries\Directions\DTO\StoreDirectionDTO;
use App\Domain\Secretaries\Directions\DTO\UpdateDirectionDTO;
use App\Domain\Secretaries\Directions\Models\Direction;
use App\Domain\Secretaries\Directions\Repositories\DirectionRepository;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DirectionController extends Controller
{
    /**
     * @var DirectionRepository
     */
    public $directions;

    /**
     * @param DirectionRepository $directionRepository
     */
    public function __construct(DirectionRepository $directionRepository)
    {
        $this->directions = $directionRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->directions->getPaginate()
            ]);
    }

    /**
     * @param Request $request
     * @param StoreDirectionAction $action
     * @return JsonResponse
     */
    public function store(Request $request, StoreDirectionAction $action): JsonResponse
    {
        try {
            $request->validate([
                'user_id' => 'required',
                'title' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreDirectionDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data created successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Direction $direction)
    {
        //
    }

    /**
     * @param Request $request
     * @param Direction $direction
     * @param UpdateDirectionAction $action
     * @return JsonResponse
     */
    public function update(Request $request, Direction $direction, UpdateDirectionAction $action): JsonResponse
    {
        try {
            $request->validate([
                'user_id' => 'required',
                'title' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $request->merge([
                'direction' => $direction
            ]);
            $dto = UpdateDirectionDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data updated successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * @param Direction $direction
     * @return JsonResponse
     */
    public function destroy(Direction $direction): JsonResponse
    {
        $direction->delete();

        return response()->json([
            'status' => true,
            'message' => 'data deleted successfully.',
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->directions->getAll(),
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function getPaginateHomePage(): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->directions->getPaginateHomePage(),
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function getRectorDirection()
    {
        return response()->json([
            'status' => true,
            'data' => $this->directions->getPaginate(),
        ]);
    }
}
