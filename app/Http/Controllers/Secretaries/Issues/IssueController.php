<?php

namespace App\Http\Controllers\Secretaries\Issues;

use App\Domain\Secretaries\Directions\Models\Direction;
use App\Domain\Secretaries\Issues\Actions\StoreIssueAction;
use App\Domain\Secretaries\Issues\Actions\UpdateIssueAction;
use App\Domain\Secretaries\Issues\DTO\StoreIssueDTO;
use App\Domain\Secretaries\Issues\DTO\UpdateIssueDTO;
use App\Domain\Secretaries\Issues\Models\Issue;
use App\Domain\Secretaries\Issues\Repositories\IssueRepository;
use App\Http\Controllers\Controller;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class IssueController extends Controller
{
    /**
     * @var IssueRepository
     */
    public $issues;

    /**
     * @param IssueRepository $issueRepository
     */
    public function __construct(IssueRepository $issueRepository)
    {
        $this->issues = $issueRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->issues->getPaginate()
            ]);
    }

    /**
     * @param Request $request
     * @param StoreIssueAction $action
     * @return JsonResponse
     * @throws Exception
     */
    public function store(Request $request, StoreIssueAction $action): JsonResponse
    {
        try {
            $request->validate([
                'direction_id' => 'required',
                'title' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreIssueDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data created successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Issue $issue)
    {
        //
    }

    /**
     * @param Request $request
     * @param Issue $issue
     * @param UpdateIssueAction $action
     * @return JsonResponse
     */
    public function update(Request $request, Issue $issue, UpdateIssueAction $action): JsonResponse
    {
        try {
            $request->validate([
                'direction_id' => 'required',
                'title' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $request->merge([
                'issue' => $issue
            ]);
            $dto = UpdateIssueDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data updated successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * @param Issue $issue
     * @return JsonResponse
     */
    public function destroy(Issue $issue): JsonResponse
    {
        $issue->delete();

        return response()->json([
            'status' => true,
            'message' => 'data deleted successfully.',
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->issues->getAll(),
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getMemberIssue(Request $request): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->issues->getMemberIssue($request)
            ]);
    }

    /**
     * @param $direction_id
     * @return JsonResponse
     */
    public function getDirectionIssue($direction_id): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->issues->getDirectionIssue($direction_id)
            ]);
    }

    public function getRectorIssue()
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->issues->getPaginate()
            ]);
    }

    public function getAgendaIssue($agenda_id)
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->issues->getAgendaIssue($agenda_id)
            ]);
    }

    /**
     * @param $agenda_id
     * @param $user_id
     * @return JsonResponse
     */
    public function getIssueRector($agenda_id,$user_id): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->issues->getIssueRector($agenda_id,$user_id)
            ]);
    }
}
