<?php

namespace App\Http\Controllers\Secretaries\Members;

use App\Domain\Secretaries\Members\Actions\StoreMemberAction;
use App\Domain\Secretaries\Members\Actions\UpdateMemberAction;
use App\Domain\Secretaries\Members\DTO\StoreMemberDTO;
use App\Domain\Secretaries\Members\DTO\UpdateMemberDTO;
use App\Domain\Secretaries\Members\Models\Member;
use App\Domain\Secretaries\Members\Repositories\MemberRepository;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class MemberController extends Controller
{
    /**
     * @var MemberRepository
     */
    public $members;

    /**
     * @param MemberRepository $memberRepository
     */
    public function __construct(MemberRepository $memberRepository)
    {
        $this->members = $memberRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->members->getPaginate()
            ]);
    }

    /**
     * @param Request $request
     * @param StoreMemberAction $action
     * @return JsonResponse
     */
    public function store(Request $request, StoreMemberAction $action): JsonResponse
    {
        try {
            $request->validate([
                'user_id' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'surname' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreMemberDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data created successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Member $member)
    {
        //
    }

    /**
     * @param Request $request
     * @param Member $member
     * @param UpdateMemberAction $action
     * @return JsonResponse
     */
    public function update(Request $request, Member $member, UpdateMemberAction $action): JsonResponse
    {
        try {
            $request->validate([
                'user_id' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'surname' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $request->merge([
                'member' => $member
            ]);
            $dto = UpdateMemberDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'data updated successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * @param Member $member
     * @return JsonResponse
     */
    public function destroy(Member $member): JsonResponse
    {
        $member->delete();

        return response()
            ->json([
                'status' => true,
                'message' => 'data deleted successfully.'
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => $this->members->getAll(),
        ]);
    }

    /**
     * @param $member_id
     * @return JsonResponse
     */
    public function updateStatus($member_id): JsonResponse
    {
        return response()->json([
            'status' => true,
            'message' => 'status updated successfully.',
            'data' => $this->members->updateStatus($member_id),
        ]);
    }

    public function getRectorMember()
    {
        return response()->json([
            'status' => true,
            'data' => $this->members->getRectorMemberPaginate(),
        ]);
    }
}
