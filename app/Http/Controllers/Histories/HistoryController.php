<?php

namespace App\Http\Controllers\Histories;

use App\Domain\Histories\Actions\StoreHistoryAction;
use App\Domain\Histories\DTO\StoreHistoryDTO;
use App\Domain\Histories\Repositories\HistoryRepository;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class HistoryController extends Controller
{
    /**
     * @var HistoryRepository
     */
    public $histories;

    /**
     * @param HistoryRepository $historyRepository
     */
    public function __construct(HistoryRepository $historyRepository)
    {
        $this->histories = $historyRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->histories->getPaginate()
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->histories->getAll()
            ]);
    }

    /**
     * @param Request $request
     * @param StoreHistoryAction $action
     * @return JsonResponse
     */
    public function store(Request $request, StoreHistoryAction $action): JsonResponse
    {
        try {
            $request->validate([
                'agenda_id' => 'required',
                'issue_id' => 'required',
                'user_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreHistoryDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'history created successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
