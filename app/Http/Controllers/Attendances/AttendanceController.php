<?php

namespace App\Http\Controllers\Attendances;

use App\Domain\Attendances\Actions\StoreAttendanceAction;
use App\Domain\Attendances\DTO\StoreAttendanceDTO;
use App\Domain\Attendances\Repositories\AttendanceRepository;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AttendanceController extends Controller
{
    /**
     * @var AttendanceRepository
     */
    public $attendances;

    /**
     * @param AttendanceRepository $attendanceRepository
     */
    public function __construct(AttendanceRepository $attendanceRepository)
    {
        $this->attendances = $attendanceRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->attendances->getPaginate()
            ]);
    }

    public function getAll(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->attendances->getAll()
            ]);
    }

    /**
     * @param $date
     * @return JsonResponse
     */
    public function getAllAttendanceCount($date): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'count' => $this->attendances->getAllAttendanceCount($date)
            ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, StoreAttendanceAction $action): JsonResponse
    {
        try {
            $request->validate([
                'agenda_id' => 'required',
                'date' => 'required',
                'user_id' => 'required',
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreAttendanceDTO::fromArray($request->all());
            $response = $action->execute($dto);
            if ($response == null) {
                return response()->json([
                    'status' => false,
                    'message' => 'Siz birmarta ovoz berdingiz.'
                ]);
            } else {
                return response()->json([
                    'status' => true,
                    'message' => 'attendance created successfully.',
                    'data' => $response
                ]);
            }
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    /**
     * @param $date
     * @return JsonResponse
     */
    public function getDateFiltr($date): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->attendances->getDateFiltr($date)
            ]);
    }
}
