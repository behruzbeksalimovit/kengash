<?php

namespace App\Http\Controllers\Questions;

use App\Domain\Questions\Actions\StoreQuestionAction;
use App\Domain\Questions\DTO\StoreQuestionDTO;
use App\Domain\Questions\Repositories\QuestionRepository;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class QuestionController extends Controller
{
    /**
     * @var QuestionRepository
     */
    public $questions;

    /**
     * @param QuestionRepository $questionRepository
     */
    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questions = $questionRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->questions->getPaginate()
            ]);
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return response()
            ->json([
                'status' => true,
                'data' => $this->questions->getAll()
            ]);
    }

    /**
     * @param Request $request
     * @param StoreQuestionAction $action
     * @return JsonResponse
     */
    public function store(Request $request, StoreQuestionAction $action): JsonResponse
    {
        try {
            $request->validate([
                'history_id' => 'required',
                'user_id' => 'required',
                'date' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreQuestionDTO::fromArray($request->all());
            $response = $action->execute($dto);

            return response()->json([
                'status' => true,
                'message' => 'question created successfully.',
                'data' => $response
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function getIssueQuestion($issue_id)
    {
        $questions = DB::table('questions')
            ->join('histories','histories.id','=','questions.history_id')
            ->join('members','members.user_id','=','questions.user_id')
            ->where('histories.issue_id','=', $issue_id)
            ->paginate(8);

        return response()
            ->json([
                'status' => true,
                'data' => $questions
            ]);
    }
}
