<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $client = Client::query()
            ->with('direction','issue')
            ->orderBy('id','desc')
            ->limit(1)
            ->get();

        return response()
            ->json([
                'status' => true,
                'data' => $client
            ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $client = new Client();
        $client->type = $request->type;
        $client->direction_id = $request->direction_id;
        $client->issue_id = $request->issue_id;
        $client->save();

        return response()
            ->json([
                'status' => true,
                'data' => $client
            ]);
    }
}
