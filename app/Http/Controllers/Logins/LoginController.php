<?php

namespace App\Http\Controllers\Logins;

use App\Domain\Secretaries\Members\Models\Member;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;

class LoginController extends Controller
{
    /**
     * @var ResponseInterface
     */
    public $response;

    /**
     * @param Request $request
     * @throws GuzzleException
     */
    public function __construct(Request $request)
    {
        $client = new \GuzzleHttp\Client();

        $this->response = json_decode($client->request('POST', 'https://uniwork.buxdu.uz/api/login.asp', [
            'form_params' => [
                'login' => $request->login,
                'parol' => $request->parol,
                'avtorizatsiya' => 1,
            ]
        ])->getBody());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        if (isset($this->response->code) == false) {
            $member = Member::query()
                ->where('user_id', '=', $this->response->id)
                ->first();
            if ($member != null && $member->active == 0) {
                $member->active = 1;
                $member->update();
            } else {
                return response()
                    ->json([
                        'status' => false,
                        'message' => 'Siz bitta qurilmadan kirishingiz mumkin'
                    ]);
            }
            return response()
                ->json([
                    'status' => true,
                    'user_id' => $this->response->id,
                    'user_fio' => $this->response->fio,
                    'user_department' => $this->response->department,
                    'user_department_name' => $this->response->department_name,
                    'user_position' => $this->response->lavozim,
                    'active' => 1,
                    '_token' => base64_encode($request->login . ':' . $request->parol . ':' . $this->response->id)
                ]);
        } else {
            return response()
                ->json([
                    'status' => false,
                    'message' => $this->response->error->message ?? 'Sizning kirishingiz ta\'qiqlangan!'
                ]);
        }

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $member = Member::query()
            ->where('user_id', '=', $request->user_id)
            ->first();
        $member->active = 0;
        $member->update();

        return response()
            ->json([
                'status' => true,
                'message' => 'success!'
            ]);
    }
}
