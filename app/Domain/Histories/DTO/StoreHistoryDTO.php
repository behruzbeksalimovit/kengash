<?php

namespace App\Domain\Histories\DTO;

class StoreHistoryDTO
{
    private int $agenda_id;
    private int $user_id;
    private int $issue_id;
    private string $start_date;
    private string $end_date;

    /**
     * @param array $data
     * @return StoreHistoryDTO
     */
    public static function fromArray(array $data): StoreHistoryDTO
    {
        $dto = new self();
        $dto->setAgendaId($data['agenda_id']);
        $dto->setUserId($data['user_id']);
        $dto->setIssueId($data['issue_id']);
        $dto->setStartDate($data['start_date']);
        $dto->setEndDate($data['end_date']);

        return $dto;
    }

    /**
     * @return int
     */
    public function getAgendaId(): int
    {
        return $this->agenda_id;
    }

    /**
     * @param int $agenda_id
     */
    public function setAgendaId(int $agenda_id): void
    {
        $this->agenda_id = $agenda_id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getIssueId(): int
    {
        return $this->issue_id;
    }

    /**
     * @param int $issue_id
     */
    public function setIssueId(int $issue_id): void
    {
        $this->issue_id = $issue_id;
    }

    /**
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->start_date;
    }

    /**
     * @param string $start_date
     */
    public function setStartDate(string $start_date): void
    {
        $this->start_date = $start_date;
    }

    /**
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->end_date;
    }

    /**
     * @param string $end_date
     */
    public function setEndDate(string $end_date): void
    {
        $this->end_date = $end_date;
    }
}
