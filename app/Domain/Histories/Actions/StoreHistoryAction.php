<?php

namespace App\Domain\Histories\Actions;

use App\Domain\Histories\DTO\StoreHistoryDTO;
use App\Domain\Histories\Models\History;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreHistoryAction
{
    /**
     * @param StoreHistoryDTO $dto
     * @return History
     * @throws Exception
     */
    public function execute(StoreHistoryDTO $dto): History
    {
        DB::beginTransaction();
        try {
            $history = new History();
            $history->agenda_id = $dto->getAgendaId();
            $history->issue_id = $dto->getIssueId();
            $history->user_id = $dto->getUserId();
            $history->start_date = $dto->getStartDate();
            $history->end_date = $dto->getEndDate();
            $history->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $history;
    }
}
