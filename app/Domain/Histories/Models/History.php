<?php

namespace App\Domain\Histories\Models;

use App\Domain\Secretaries\Agendas\Models\Agenda;
use App\Domain\Secretaries\Issues\Models\Issue;
use App\Domain\Secretaries\Members\Models\Member;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;

    protected $perPage = 15;

    public function agenda()
    {
        return $this->belongsTo(Agenda::class);
    }

    public function issue()
    {
        return $this->belongsTo(Issue::class);
    }

    public function user()
    {
        return $this->hasOne(Member::class,'user_id','user_id');
    }
}
