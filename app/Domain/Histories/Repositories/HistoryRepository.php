<?php

namespace App\Domain\Histories\Repositories;

use App\Domain\Histories\Models\History;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class HistoryRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return History::query()
            ->with(['agenda','issue'])
            ->orderBy('id','desc')
            ->paginate();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll(): Collection|array
    {
        return History::query()
            ->with(['agenda','issue'])
            ->orderBy('id','desc')
            ->get();
    }
}
