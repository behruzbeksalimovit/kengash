<?php

namespace App\Domain\Traits;

use App\Domain\Secretaries\Members\Models\Member;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

trait LoginTrait
{
    /**
     * @param $request
     * @return mixed
     * @throws GuzzleException
     */
    public function login($request): mixed
    {
        $user = explode(':', base64_decode(substr($request->header('Authorization'), 6)));
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://uniwork.buxdu.uz/api/login.asp', [
            'form_params' => [
                'login' => $user[0],
                'parol' => $user[1],
                'avtorizatsiya' => 1,
            ]
        ]);
        return json_decode($response->getBody());
    }
}
