<?php

namespace App\Domain\Members\Infos\DTO;

use App\Domain\Members\Infos\Models\Info;

class UpdateInfoDTO
{
    /**
     * @var int
     */
    private int $issue_id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $description;

    /**
     * @var string
     */
    private string $decision;

    /**
     * @var array
     */
    private array $decisions;

    /**
     * @var Info
     */
    private Info $info;

    /**
     * @param array $data
     * @return UpdateInfoDTO
     */
    public static function fromArray(array $data): UpdateInfoDTO
    {
        $dto = new self();
        $dto->setIssueId($data['issue_id']);
        $dto->setTitle($data['title']);
        $dto->setDescription($data['description']);
        $dto->setDecision($data['decision']);
        $dto->setDecisions($data['decisions']);
        $dto->setInfo($data['info']);

        return $dto;
    }

    /**
     * @return array
     */
    public function getDecisions(): array
    {
        return $this->decisions;
    }

    /**
     * @param array $decisions
     */
    public function setDecisions(array $decisions): void
    {
        $this->decisions = $decisions;
    }

    /**
     * @return int
     */
    public function getIssueId(): int
    {
        return $this->issue_id;
    }

    /**
     * @param int $issue_id
     */
    public function setIssueId(int $issue_id): void
    {
        $this->issue_id = $issue_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDecision(): string
    {
        return $this->decision;
    }

    /**
     * @param string $decision
     */
    public function setDecision(string $decision): void
    {
        $this->decision = $decision;
    }

    /**
     * @return Info
     */
    public function getInfo(): Info
    {
        return $this->info;
    }

    /**
     * @param Info $info
     */
    public function setInfo(Info $info): void
    {
        $this->info = $info;
    }
}
