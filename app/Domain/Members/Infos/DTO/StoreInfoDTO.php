<?php

namespace App\Domain\Members\Infos\DTO;

class StoreInfoDTO
{
    /**
     * @var int
     */
    private int $issue_id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $description;

    /**
     * @var string
     */
    private string $decision;

    /**
     * @var array
     */
    private array $decisions;

    /**
     * @param array $data
     * @return StoreInfoDTO
     */
    public static function fromArray(array $data): StoreInfoDTO
    {
        $dto = new self();
        $dto->setIssueId($data['issue_id']);
        $dto->setTitle($data['title']);
        $dto->setDescription($data['description']);
        $dto->setDecision($data['decision']);
        $dto->setDecisions($data['decisions']);

        return $dto;
    }

    /**
     * @return array
     */
    public function getDecisions(): array
    {
        return $this->decisions;
    }

    /**
     * @param array $decisions
     */
    public function setDecisions(array $decisions): void
    {
        $this->decisions = $decisions;
    }

    /**
     * @return int
     */
    public function getIssueId(): int
    {
        return $this->issue_id;
    }

    /**
     * @param int $issue_id
     */
    public function setIssueId(int $issue_id): void
    {
        $this->issue_id = $issue_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDecision(): string
    {
        return $this->decision;
    }

    /**
     * @param string $decision
     */
    public function setDecision(string $decision): void
    {
        $this->decision = $decision;
    }
}
