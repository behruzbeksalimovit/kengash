<?php

namespace App\Domain\Members\Infos\Repositories;

use App\Domain\Members\Infos\Models\Info;
use App\Domain\Traits\LoginTrait;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class InfoRepository
{
    use LoginTrait;

    /**
     * @param $request
     * @return LengthAwarePaginator
     * @throws GuzzleException
     */
    public function getPaginate($request): LengthAwarePaginator
    {
        $result = $this->login($request);
        return Info::query()
            ->with(['issue', 'issue.direction'])
            ->whereHas('issue', function ($query) use ($result) {
                $query->where('user_id', '=', $result->id);
            })
            ->orderBy('id', 'desc')
            ->paginate();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll(): Collection|array
    {
        return Info::query()
            ->with('issue')
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @param $issue_id
     * @return LengthAwarePaginator
     */
    public function getMemberAgenda($issue_id): LengthAwarePaginator
    {
        return Info::query()
            ->where('issue_id', '=', $issue_id)
            ->orderBy('id', 'desc')
            ->paginate();
    }

    /**
     * @param $issue_id
     * @param $user_id
     * @return LengthAwarePaginator
     */
    public function getMemberIssueRectorAll($issue_id, $user_id): LengthAwarePaginator
    {
        return Info::query()
            ->with(['issue', 'issue.direction' => function ($query) use ($user_id) {
                $query->where('user_id', '=', $user_id)->get();
            }])
            ->where('issue_id', '=', $issue_id)
            ->orderBy('id', 'desc')
            ->paginate(20);
    }

    /**
     * @param $issue_id
     * @return LengthAwarePaginator
     */
    public function getIssueInfo($issue_id): LengthAwarePaginator
    {
        return Info::query()
            ->with(['issue'])
            ->where('issue_id', '=', $issue_id)
            ->orderBy('id', 'desc')
            ->paginate(10);
    }
}
