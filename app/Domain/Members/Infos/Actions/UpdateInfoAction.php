<?php

namespace App\Domain\Members\Infos\Actions;

use App\Domain\Members\Infos\DTO\StoreInfoDTO;
use App\Domain\Members\Infos\DTO\UpdateInfoDTO;
use App\Domain\Members\Infos\Models\Info;
use Exception;
use Illuminate\Support\Facades\DB;

class UpdateInfoAction
{
    /**
     * @param UpdateInfoDTO $dto
     * @return Info
     * @throws Exception
     */
    public function execute(UpdateInfoDTO $dto): Info
    {
        DB::beginTransaction();
        try {
            $info = $dto->getInfo();
            $info->issue_id = $dto->getIssueId();
            $info->title = $dto->getTitle();
            $info->decisions = $dto->getDecisions();
            $info->description = $dto->getDescription();
            $info->decision = $dto->getDecision();
            $info->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $info;
    }
}
