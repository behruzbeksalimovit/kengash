<?php

namespace App\Domain\Members\Infos\Actions;

use App\Domain\Members\Infos\DTO\StoreInfoDTO;
use App\Domain\Members\Infos\Models\Info;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreInfoAction
{
    /**
     * @param StoreInfoDTO $dto
     * @return Info
     * @throws Exception
     */
    public function execute(StoreInfoDTO $dto): Info
    {
        DB::beginTransaction();
        try {
            $info = new Info();
            $info->issue_id = $dto->getIssueId();
            $info->title = $dto->getTitle();
            $info->decisions = $dto->getDecisions();
            $info->description = $dto->getDescription();
            $info->decision = $dto->getDecision();
            $info->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $info;
    }
}
