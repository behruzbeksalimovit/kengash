<?php

namespace App\Domain\Members\Infos\Models;

use App\Domain\Secretaries\Issues\Models\Issue;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Info extends Model
{
    use HasFactory;

    protected $perPage = 15;

    protected $casts = [
        'decisions' => 'json',
    ];

    /**
     * Get the user's first name.
     *
     * @return Attribute
     */
    protected function decisions(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => json_decode($value, true),
            set: fn ($value) => json_encode($value),
        );
    }

    /**
     * @return BelongsTo
     */
    public function issue(): BelongsTo
    {
        return $this->belongsTo(Issue::class);
    }
}
