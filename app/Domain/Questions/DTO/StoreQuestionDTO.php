<?php

namespace App\Domain\Questions\DTO;

class StoreQuestionDTO
{
    private int $user_id;
    private int $history_id;
    private string $date;
    private ?string $desc = null;

    /**
     * @param array $data
     * @return StoreQuestionDTO
     */
    public static function fromArray(array $data): StoreQuestionDTO
    {
        $dto = new self();
        $dto->setUserId($data['user_id']);
        $dto->setHistoryId($data['history_id']);
        $dto->setDate($data['date']);
        $dto->setDesc($data['desc'] ?? null);

        return $dto;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getHistoryId(): int
    {
        return $this->history_id;
    }

    /**
     * @param int $history_id
     */
    public function setHistoryId(int $history_id): void
    {
        $this->history_id = $history_id;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string|null
     */
    public function getDesc(): ?string
    {
        return $this->desc;
    }

    /**
     * @param string|null $desc
     */
    public function setDesc(?string $desc): void
    {
        $this->desc = $desc;
    }
}
