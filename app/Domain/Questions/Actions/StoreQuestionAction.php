<?php

namespace App\Domain\Questions\Actions;

use App\Domain\Questions\DTO\StoreQuestionDTO;
use App\Domain\Questions\Models\Question;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreQuestionAction
{
    /**
     * @param StoreQuestionDTO $dto
     * @return Question
     * @throws Exception
     */
    public function execute(StoreQuestionDTO $dto): Question
    {
        DB::beginTransaction();
        try {
            $question = new Question();
            $question->user_id = $dto->getUserId();
            $question->history_id = $dto->getHistoryId();
            $question->date = $dto->getDate();
            $question->desc = $dto->getDesc();
            $question->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $question;
    }
}
