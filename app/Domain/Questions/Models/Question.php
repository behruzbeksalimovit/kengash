<?php

namespace App\Domain\Questions\Models;

use App\Domain\Secretaries\Members\Models\Member;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $perPage = 15;

    public function user()
    {
        return $this->hasOne(Member::class,'user_id','user_id');
    }
}
