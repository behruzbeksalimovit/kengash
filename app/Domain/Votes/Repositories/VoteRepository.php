<?php

namespace App\Domain\Votes\Repositories;

use App\Domain\Secretaries\Members\Models\Member;
use App\Domain\Votes\Models\Vote;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class VoteRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return Vote::query()
            ->orderBy('id','desc')
            ->paginate();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll(): Collection|array
    {
        return Vote::query()
            ->orderBy('id','desc')
            ->get();
    }

    /**
     * @param $agenda_id
     * @return int
     */
    public function tarafdorlar($agenda_id): int
    {
        return Vote::query()
            ->where('agenda_id','=', $agenda_id)
            ->where('issue_id','=', null)
            ->where('status','=', 1)
            ->count();
    }

    /**
     * @param $agenda_id
     * @return int
     */
    public function betaraflar($agenda_id): int
    {
        return Vote::query()
            ->where('agenda_id','=', $agenda_id)
            ->where('issue_id','=', null)
            ->where('status','=', 2)
            ->count();
    }

    /**
     * @param $agenda_id
     * @return int
     */
    public function qarshilar($agenda_id): int
    {
        return Vote::query()
            ->where('agenda_id','=', $agenda_id)
            ->where('issue_id','=', null)
            ->where('status','=', 3)
            ->count();
    }

    /**
     * @param $agenda_id
     * @return int
     */
    public function ovozbermaganlar($agenda_id)
    {
        return Member::select('id','user_id')
            ->where('user_id','!=', env('RECTOR'))
            ->count() - ($this->tarafdorlar($agenda_id) + $this->betaraflar($agenda_id) + $this->qarshilar($agenda_id));
    }

    /**
     * @param $agenda_id
     * @return int
     */
    public function jami($agenda_id)
    {
        return ($this->ovozbermaganlar($agenda_id) + $this->tarafdorlar($agenda_id) + $this->betaraflar($agenda_id) + $this->qarshilar($agenda_id));
    }

    public function filtrIssue($issue_id)
    {
        return Vote::query()
            ->with(['issue'])
            ->where('issue_id','=',$issue_id)
            ->paginate(10);
    }


    public function getVoteIssueTarafdorlar($issue_id)
    {
        return Vote::query()
            ->with(['issue'])
            ->where('issue_id','=',$issue_id)
            ->where('status','=',1)
            ->count();
    }

    public function getVoteIssueBetaraflar($issue_id)
    {
        return Vote::query()
            ->with(['issue'])
            ->where('issue_id','=',$issue_id)
            ->where('status','=',2)
            ->count();
    }

    public function getVoteIssueQarshilar($issue_id)
    {
        return Vote::query()
            ->with(['issue'])
            ->where('issue_id','=',$issue_id)
            ->where('status','=',3)
            ->count();
    }

    public function getVoteIssueOvozBermaganlar($issue_id)
    {
        return Vote::query()
            ->with(['issue'])
            ->where('issue_id','=',$issue_id)
            ->count() - ($this->getVoteIssueTarafdorlar($issue_id) + $this->getVoteIssueBetaraflar($issue_id) + $this->getVoteIssueQarshilar($issue_id));
    }

    public function getVoteIssueJami($issue_id)
    {
        return $this->getVoteIssueTarafdorlar($issue_id) + $this->getVoteIssueBetaraflar($issue_id) + $this->getVoteIssueQarshilar($issue_id);
    }
}
