<?php

namespace App\Domain\Votes\DTO;

class StoreVoteDTO
{
    /**
     * @var int
     */
    private int $agenda_id;

    /**
     * @var int|null
     */
    private ?int $issue_id = null;

    /**
     * @var int
     */
    private int $user_id;

    /**
     * @var int
     */
    private int $status;

    /**
     * @var int|null
     */
    private ?int $disabled = null;

    /**
     * @param array $data
     * @return StoreVoteDTO
     */
    public static function fromArray(array $data): StoreVoteDTO
    {
        $dto = new self();
        $dto->setAgendaId($data['agenda_id']);
        $dto->setUserId($data['user_id']);
        $dto->setIssueId($data['issue_id'] ?? null);
        $dto->setDisabled($data['disabled'] ?? null);
        $dto->setStatus($data['status']);

        return $dto;
    }

    /**
     * @return int
     */
    public function getAgendaId(): int
    {
        return $this->agenda_id;
    }

    /**
     * @param int $agenda_id
     */
    public function setAgendaId(int $agenda_id): void
    {
        $this->agenda_id = $agenda_id;
    }

    /**
     * @return int|null
     */
    public function getIssueId(): ?int
    {
        return $this->issue_id;
    }

    /**
     * @param int|null $issue_id
     */
    public function setIssueId(?int $issue_id): void
    {
        $this->issue_id = $issue_id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getDisabled(): ?int
    {
        return $this->disabled;
    }

    /**
     * @param int|null $disabled
     */
    public function setDisabled(?int $disabled): void
    {
        $this->disabled = $disabled;
    }
}
