<?php

namespace App\Domain\Votes\Models;

use App\Domain\Secretaries\Agendas\Models\Agenda;
use App\Domain\Secretaries\Issues\Models\Issue;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    use HasFactory;

    protected $perPage = 15;

    public function issue()
    {
        return $this->belongsTo(Issue::class);
    }

    public function agenda()
    {
        return $this->belongsTo(Agenda::class);
    }
}
