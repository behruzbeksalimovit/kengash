<?php

namespace App\Domain\Votes\Actions;

use App\Domain\Votes\DTO\StoreVoteDTO;
use App\Domain\Votes\Models\Vote;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreVoteAction
{
    /**
     * @param StoreVoteDTO $dto
     * @return Vote|null
     * @throws Exception
     */
    public function execute(StoreVoteDTO $dto): ?Vote
    {
        DB::beginTransaction();
        try {
            $current_vote_agenda = Vote::query()
                ->with('agenda')
                ->whereHas('agenda', function ($query){
                    $query->where('lifetime','=', date('F d, Y'));
                })
                ->where('agenda_id', '=', $dto->getAgendaId())
                ->where('user_id', '=', $dto->getUserId())
                ->where('issue_id', '=', null)
                ->first();
            $current_vote_issue = Vote::query()
                ->with('agenda')
                ->whereHas('agenda', function ($query){
                    $query->where('lifetime','=', date('F d, Y'));
                })
                ->where('agenda_id', '=', $dto->getAgendaId())
                ->where('user_id', '=', $dto->getUserId())
                ->where('issue_id', '=', $dto->getIssueId())
                ->first();

            if($dto->getIssueId() == null){
                if ($current_vote_agenda == null) {
                    $vote = new Vote();
                    $vote->agenda_id = $dto->getAgendaId();
                    $vote->issue_id = $dto->getIssueId();
                    $vote->user_id = $dto->getUserId();
                    $vote->status = $dto->getStatus();
                    $vote->disabled = $dto->getDisabled();
                    $vote->save();
                }
            }else{
                if ($current_vote_issue == null) {
                    $vote = new Vote();
                    $vote->agenda_id = $dto->getAgendaId();
                    $vote->issue_id = $dto->getIssueId();
                    $vote->user_id = $dto->getUserId();
                    $vote->status = $dto->getStatus();
                    $vote->disabled = $dto->getDisabled();
                    $vote->save();
                }
            }
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $vote ?? null;
    }
}
