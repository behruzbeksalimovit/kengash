<?php

namespace App\Domain\Attendances\Models;

use App\Domain\Secretaries\Agendas\Models\Agenda;
use App\Domain\Secretaries\Members\Models\Member;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;

    protected $perPage = 15;

    public function agenda()
    {
        return $this->belongsTo(Agenda::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class,'user_id','user_id');
    }
}
