<?php

namespace App\Domain\Attendances\Repositories;

use App\Domain\Attendances\Models\Attendance;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class AttendanceRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return Attendance::query()
            ->orderBy('id','desc')
            ->paginate();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll(): Collection|array
    {
        return Attendance::query()
            ->with(['agenda','member'])
            ->orderBy('id','desc')
            ->get();
    }

    /**
     * @param $date
     * @return int
     */
    public function getAllAttendanceCount($date): int
    {
        return Attendance::query()
            ->select('id','date')
            ->where('date','=',$date)
            ->count();
    }

    /**
     * @param $date
     * @return LengthAwarePaginator
     */
    public function getDateFiltr($date): LengthAwarePaginator
    {
        return Attendance::query()
            ->with(['agenda','member'])
            ->where('date','=',$date)
            ->paginate(20);
    }
}
