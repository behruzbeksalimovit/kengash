<?php

namespace App\Domain\Attendances\Actions;

use App\Domain\Attendances\DTO\StoreAttendanceDTO;
use App\Domain\Attendances\Models\Attendance;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreAttendanceAction
{
    /**
     * @param StoreAttendanceDTO $dto
     * @return Attendance
     * @throws Exception
     */
    public function execute(StoreAttendanceDTO $dto)
    {
        DB::beginTransaction();
        try {
                $current_attendance = Attendance::query()
                    ->with('agenda')
                    ->whereHas('agenda', function ($query){
                        $query->where('lifetime','=', date('F d, Y'));
                    })
                    ->where('user_id', '=', $dto->getUserId())
                    ->where('date', '=', $dto->getDate())
                    ->where('agenda_id', '=', $dto->getAgendaId())
                    ->first();
            if ($current_attendance == null) {
                $attendance = new Attendance();
                $attendance->agenda_id = $dto->getAgendaId();
                $attendance->date = $dto->getDate();
                $attendance->user_id = $dto->getUserId();
                $attendance->save();
            } else {
                $attendance = null;
            }
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $attendance;
    }
}
