<?php

namespace App\Domain\Attendances\DTO;

class StoreAttendanceDTO
{
    /**
     * @var int
     */
    private int $agenda_id;

    /**
     * @var string
     */
    private string $date;

    /**
     * @var int
     */
    private int $user_id;

    /**
     * @param array $data
     * @return StoreAttendanceDTO
     */
    public static function fromArray(array $data): StoreAttendanceDTO
    {
        $dto = new self();
        $dto->setAgendaId($data['agenda_id']);
        $dto->setUserId($data['user_id']);
        $dto->setDate($data['date']);

        return $dto;
    }

    /**
     * @return int
     */
    public function getAgendaId(): int
    {
        return $this->agenda_id;
    }

    /**
     * @param int $agenda_id
     */
    public function setAgendaId(int $agenda_id): void
    {
        $this->agenda_id = $agenda_id;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }
}
