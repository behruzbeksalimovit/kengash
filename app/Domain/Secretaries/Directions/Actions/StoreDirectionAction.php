<?php

namespace App\Domain\Secretaries\Directions\Actions;

use App\Domain\Secretaries\Directions\DTO\StoreDirectionDTO;
use App\Domain\Secretaries\Directions\Models\Direction;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreDirectionAction
{
    /**
     * @param StoreDirectionDTO $dto
     * @return Direction
     * @throws Exception
     */
    public function execute(StoreDirectionDTO $dto): Direction
    {
        DB::beginTransaction();
        try {
            $direction = new Direction();
            $direction->user_id = $dto->getUserId();
            $direction->title = $dto->getTitle();
            $direction->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $direction;
    }
}
