<?php

namespace App\Domain\Secretaries\Directions\Actions;

use App\Domain\Secretaries\Directions\DTO\StoreDirectionDTO;
use App\Domain\Secretaries\Directions\DTO\UpdateDirectionDTO;
use App\Domain\Secretaries\Directions\Models\Direction;
use Exception;
use Illuminate\Support\Facades\DB;

class UpdateDirectionAction
{
    /**
     * @param UpdateDirectionDTO $dto
     * @return Direction
     * @throws Exception
     */
    public function execute(UpdateDirectionDTO $dto): Direction
    {
        DB::beginTransaction();
        try {
            $direction = $dto->getDirection();
            $direction->user_id = $dto->getUserId();
            $direction->title = $dto->getTitle();
            $direction->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $direction;
    }
}
