<?php

namespace App\Domain\Secretaries\Directions\Repositories;

use App\Domain\Secretaries\Directions\Models\Direction;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class DirectionRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return Direction::query()
            ->with(['member'])
            ->orderBy('id','desc')
            ->paginate();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll(): Collection|array
    {
        return Direction::query()
            ->with(['member'])
            ->orderBy('id','desc')
            ->get();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getPaginateHomePage(): LengthAwarePaginator
    {
        return Direction::query()
            ->with(['member'])
            ->orderBy('id','desc')
            ->paginate(8);
    }
}
