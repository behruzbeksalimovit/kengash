<?php

namespace App\Domain\Secretaries\Directions\DTO;

use App\Domain\Secretaries\Directions\Models\Direction;

class UpdateDirectionDTO
{
    /**
     * @var int
     */
    private int $user_id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var Direction
     */
    private Direction $direction;

    /**
     * @param array $data
     * @return UpdateDirectionDTO
     */
    public static function fromArray(array $data): UpdateDirectionDTO
    {
        $dto = new self();
        $dto->setUserId($data['user_id']);
        $dto->setTitle($data['title']);
        $dto->setDirection($data['direction']);
        return $dto;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Direction
     */
    public function getDirection(): Direction
    {
        return $this->direction;
    }

    /**
     * @param Direction $direction
     */
    public function setDirection(Direction $direction): void
    {
        $this->direction = $direction;
    }
}
