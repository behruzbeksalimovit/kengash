<?php

namespace App\Domain\Secretaries\Directions\DTO;

class StoreDirectionDTO
{
    /**
     * @var int
     */
    private int $user_id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @param array $data
     * @return StoreDirectionDTO
     */
    public static function fromArray(array $data): StoreDirectionDTO
    {
        $dto = new self();
        $dto->setUserId($data['user_id']);
        $dto->setTitle($data['title']);
        return $dto;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }
}
