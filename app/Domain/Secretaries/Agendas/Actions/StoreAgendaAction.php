<?php

namespace App\Domain\Secretaries\Agendas\Actions;

use App\Domain\Secretaries\Agendas\DTO\StoreAgendaDTO;
use App\Domain\Secretaries\Agendas\Models\Agenda;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreAgendaAction
{
    /**
     * @param StoreAgendaDTO $dto
     * @return Agenda
     * @throws Exception
     */
    public function execute(StoreAgendaDTO $dto): Agenda
    {
        DB::beginTransaction();
        try {
            $agenda = new Agenda();
            $agenda->title = $dto->getTitle();
            $agenda->lifetime = $dto->getLifetime();
            $agenda->input_time = $dto->getInputTime();
            $agenda->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $agenda;
    }
}
