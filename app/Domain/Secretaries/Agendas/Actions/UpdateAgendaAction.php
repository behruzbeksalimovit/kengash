<?php

namespace App\Domain\Secretaries\Agendas\Actions;

use App\Domain\Secretaries\Agendas\DTO\StoreAgendaDTO;
use App\Domain\Secretaries\Agendas\DTO\UpdateAgendaDTO;
use App\Domain\Secretaries\Agendas\Models\Agenda;
use Exception;
use Illuminate\Support\Facades\DB;

class UpdateAgendaAction
{
    /**
     * @param UpdateAgendaDTO $dto
     * @return Agenda
     * @throws Exception
     */
    public function execute(UpdateAgendaDTO $dto): Agenda
    {
        DB::beginTransaction();
        try {
            $agenda = $dto->getAgenda();
            $agenda->title = $dto->getTitle();
            $agenda->lifetime = $dto->getLifetime();
            $agenda->input_time = $dto->getInputTime();
            $agenda->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $agenda;
    }
}
