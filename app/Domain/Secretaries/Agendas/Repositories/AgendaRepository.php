<?php

namespace App\Domain\Secretaries\Agendas\Repositories;

use App\Domain\Secretaries\Agendas\Models\Agenda;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class AgendaRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return Agenda::query()
            ->where('lifetime','=', date('F d, Y'))
            ->orderBy('id', 'desc')
            ->paginate();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll()
    {
        return Agenda::query()
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getPaginateHomePage(): LengthAwarePaginator
    {
        return Agenda::query()
            ->where('lifetime','=', date('F d, Y'))
            ->orderBy('id','desc')
            ->paginate(10);
    }
}
