<?php

namespace App\Domain\Secretaries\Agendas\DTO;

use App\Domain\Secretaries\Agendas\Models\Agenda;

class UpdateAgendaDTO
{
    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $lifetime;

    /**
     * @var string
     */
    private string $input_time;

    /**
     * @var Agenda
     */
    private Agenda $agenda;

    /**
     * @param array $data
     * @return UpdateAgendaDTO
     */
    public static function fromArray(array $data): UpdateAgendaDTO
    {
        $dto = new self();
        $dto->setTitle($data['title']);
        $dto->setLifetime($data['lifetime']);
        $dto->setInputTime($data['input_time']);
        $dto->setAgenda($data['agenda']);

        return $dto;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLifetime(): string
    {
        return $this->lifetime;
    }

    /**
     * @param string $lifetime
     */
    public function setLifetime(string $lifetime): void
    {
        $this->lifetime = $lifetime;
    }

    /**
     * @return string
     */
    public function getInputTime(): string
    {
        return $this->input_time;
    }

    /**
     * @param string $input_time
     */
    public function setInputTime(string $input_time): void
    {
        $this->input_time = $input_time;
    }

    /**
     * @return Agenda
     */
    public function getAgenda(): Agenda
    {
        return $this->agenda;
    }

    /**
     * @param Agenda $agenda
     */
    public function setAgenda(Agenda $agenda): void
    {
        $this->agenda = $agenda;
    }
}
