<?php

namespace App\Domain\Secretaries\Agendas\DTO;

class StoreAgendaDTO
{
    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $lifetime;

    /**
     * @var string
     */
    private string $input_time;

    /**
     * @param array $data
     * @return StoreAgendaDTO
     */
    public static function fromArray(array $data): StoreAgendaDTO
    {
        $dto = new self();
        $dto->setTitle($data['title']);
        $dto->setLifetime($data['lifetime']);
        $dto->setInputTime($data['input_time']);

        return $dto;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLifetime(): string
    {
        return $this->lifetime;
    }

    /**
     * @param string $lifetime
     */
    public function setLifetime(string $lifetime): void
    {
        $this->lifetime = $lifetime;
    }

    /**
     * @return string
     */
    public function getInputTime(): string
    {
        return $this->input_time;
    }

    /**
     * @param string $input_time
     */
    public function setInputTime(string $input_time): void
    {
        $this->input_time = $input_time;
    }
}
