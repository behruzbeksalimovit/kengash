<?php

namespace App\Domain\Secretaries\Members\DTO;

class StoreMemberDTO
{
    /**
     * @var int
     */
    private int $user_id;

    /**
     * @var string
     */
    private string $firstname;

    /**
     * @var string
     */
    private string $lastname;

    /**
     * @var string
     */
    private string $surname;

    /**
     * @param array $data
     * @return StoreMemberDTO
     */
    public static function fromArray(array $data): StoreMemberDTO
    {
        $dto = new self();
        $dto->setUserId($data['user_id']);
        $dto->setFirstname($data['firstname']);
        $dto->setLastname($data['lastname']);
        $dto->setSurname($data['surname']);
        return $dto;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }
}
