<?php

namespace App\Domain\Secretaries\Members\Repositories;

use App\Domain\Secretaries\Members\Models\Member;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class MemberRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return Member::query()
//            ->orderBy('id', 'desc')
            ->paginate();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll(): Collection|array
    {
        return Member::query()
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @param $member_id
     * @return Model|Builder|Builder[]|Collection
     */
    public function updateStatus($member_id): Builder|array|Collection|Model
    {
        $user = Member::query()
            ->find($member_id);

        if ($user->status == true) {
            $user->status = false;
        } elseif ($user->status == false) {
            $user->status = true;
        }
        $user->update();
        return $user;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getRectorMemberPaginate()
    {
        return Member::query()
            ->whereNotIn('user_id',  [env('RECTOR'),env('MEMBER_TEST'),env('SECRETARY_TEST')])
            ->paginate(15);
    }
}
