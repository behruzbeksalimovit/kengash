<?php

namespace App\Domain\Secretaries\Members\Actions;

use App\Domain\Secretaries\Members\DTO\StoreMemberDTO;
use App\Domain\Secretaries\Members\Models\Member;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreMemberAction
{
    /**
     * @param StoreMemberDTO $dto
     * @return Member
     * @throws Exception
     */
    public function execute(StoreMemberDTO $dto): Member
    {
        DB::beginTransaction();
        try {
            $member = new Member();
            $member->user_id = $dto->getUserId();
            $member->firstname = $dto->getFirstname();
            $member->lastname = $dto->getLastname();
            $member->surname = $dto->getSurname();
            $member->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $member;
    }
}
