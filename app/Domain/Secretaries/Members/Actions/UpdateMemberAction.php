<?php

namespace App\Domain\Secretaries\Members\Actions;

use App\Domain\Secretaries\Members\DTO\StoreMemberDTO;
use App\Domain\Secretaries\Members\DTO\UpdateMemberDTO;
use App\Domain\Secretaries\Members\Models\Member;
use Exception;
use Illuminate\Support\Facades\DB;

class UpdateMemberAction
{
    /**
     * @param UpdateMemberDTO $dto
     * @return Member
     * @throws Exception
     */
    public function execute(UpdateMemberDTO $dto): Member
    {
        DB::beginTransaction();
        try {
            $member = $dto->getMember();
            $member->user_id = $dto->getUserId();
            $member->firstname = $dto->getFirstname();
            $member->lastname = $dto->getLastname();
            $member->surname = $dto->getSurname();
            $member->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $member;
    }
}
