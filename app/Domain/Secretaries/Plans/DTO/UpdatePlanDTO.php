<?php

namespace App\Domain\Secretaries\Plans\DTO;

use App\Domain\Secretaries\Plans\Models\Plan;

class UpdatePlanDTO
{
    /**
     * @var int
     */
    private int $responsible_id;

    /**
     * @var int
     */
    private int $reporter_id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $responsible;

    /**
     * @var string
     */
    private string $lifetime;

    /**
     * @var string
     */
    private string $reporter;

    /**
     * @var Plan
     */
    private Plan $plan;

    /**
     * @param array $data
     * @return UpdatePlanDTO
     */
    public static function fromArray(array $data): UpdatePlanDTO
    {
        $dto = new self();
        $dto->setResponsibleId($data['responsible_id']);
        $dto->setReporterId($data['reporter_id']);
        $dto->setTitle($data['title']);
        $dto->setResponsible($data['responsible']);
        $dto->setLifetime($data['lifetime']);
        $dto->setReporter($data['reporter']);
        $dto->setPlan($data['plan']);

        return $dto;
    }

    /**
     * @return int
     */
    public function getResponsibleId(): int
    {
        return $this->responsible_id;
    }

    /**
     * @param int $responsible_id
     */
    public function setResponsibleId(int $responsible_id): void
    {
        $this->responsible_id = $responsible_id;
    }

    /**
     * @return int
     */
    public function getReporterId(): int
    {
        return $this->reporter_id;
    }

    /**
     * @param int $reporter_id
     */
    public function setReporterId(int $reporter_id): void
    {
        $this->reporter_id = $reporter_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getResponsible(): string
    {
        return $this->responsible;
    }

    /**
     * @param string $responsible
     */
    public function setResponsible(string $responsible): void
    {
        $this->responsible = $responsible;
    }

    /**
     * @return string
     */
    public function getLifetime(): string
    {
        return $this->lifetime;
    }

    /**
     * @param string $lifetime
     */
    public function setLifetime(string $lifetime): void
    {
        $this->lifetime = $lifetime;
    }

    /**
     * @return string
     */
    public function getReporter(): string
    {
        return $this->reporter;
    }

    /**
     * @param string $reporter
     */
    public function setReporter(string $reporter): void
    {
        $this->reporter = $reporter;
    }

    /**
     * @return Plan
     */
    public function getPlan(): Plan
    {
        return $this->plan;
    }

    /**
     * @param Plan $plan
     */
    public function setPlan(Plan $plan): void
    {
        $this->plan = $plan;
    }
}
