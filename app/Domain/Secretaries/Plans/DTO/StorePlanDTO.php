<?php

namespace App\Domain\Secretaries\Plans\DTO;

class StorePlanDTO
{
    /**
     * @var int
     */
    private int $responsible_id;

    /**
     * @var int
     */
    private int $reporter_id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $responsible;

    /**
     * @var string
     */
    private string $lifetime;

    /**
     * @var string
     */
    private string $reporter;

    /**
     * @param array $data
     * @return StorePlanDTO
     */
    public static function fromArray(array $data): StorePlanDTO
    {
        $dto = new self();
        $dto->setResponsibleId($data['responsible_id']);
        $dto->setReporterId($data['reporter_id']);
        $dto->setTitle($data['title']);
        $dto->setResponsible($data['responsible']);
        $dto->setLifetime($data['lifetime']);
        $dto->setReporter($data['reporter']);

        return $dto;
    }

    /**
     * @return int
     */
    public function getResponsibleId(): int
    {
        return $this->responsible_id;
    }

    /**
     * @param int $responsible_id
     */
    public function setResponsibleId(int $responsible_id): void
    {
        $this->responsible_id = $responsible_id;
    }

    /**
     * @return int
     */
    public function getReporterId(): int
    {
        return $this->reporter_id;
    }

    /**
     * @param int $reporter_id
     */
    public function setReporterId(int $reporter_id): void
    {
        $this->reporter_id = $reporter_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getResponsible(): string
    {
        return $this->responsible;
    }

    /**
     * @param string $responsible
     */
    public function setResponsible(string $responsible): void
    {
        $this->responsible = $responsible;
    }

    /**
     * @return string
     */
    public function getLifetime(): string
    {
        return $this->lifetime;
    }

    /**
     * @param string $lifetime
     */
    public function setLifetime(string $lifetime): void
    {
        $this->lifetime = $lifetime;
    }

    /**
     * @return string
     */
    public function getReporter(): string
    {
        return $this->reporter;
    }

    /**
     * @param string $reporter
     */
    public function setReporter(string $reporter): void
    {
        $this->reporter = $reporter;
    }
}
