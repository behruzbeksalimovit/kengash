<?php

namespace App\Domain\Secretaries\Plans\Repositories;

use App\Domain\Secretaries\Plans\Models\Plan;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class PlanRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return Plan::query()
            ->orderBy('id','desc')
            ->paginate();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll()
    {
        return Plan::query()
            ->orderBy('id','desc')
            ->get();
    }
}
