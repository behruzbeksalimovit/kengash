<?php

namespace App\Domain\Secretaries\Plans\Actions;

use App\Domain\Secretaries\Plans\DTO\StorePlanDTO;
use App\Domain\Secretaries\Plans\Models\Plan;
use Exception;
use Illuminate\Support\Facades\DB;

class StorePlanAction
{
    /**
     * @param StorePlanDTO $dto
     * @return Plan
     * @throws Exception
     */
    public function execute(StorePlanDTO $dto): Plan
    {
        DB::beginTransaction();
        try {
            $plan = new Plan();
            $plan->responsible_id = $dto->getResponsibleId();
            $plan->reporter_id = $dto->getReporterId();
            $plan->title = $dto->getTitle();
            $plan->responsible = $dto->getResponsible();
            $plan->lifetime = $dto->getLifetime();
            $plan->reporter = $dto->getReporter();
            $plan->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();

        return $plan;
    }
}
