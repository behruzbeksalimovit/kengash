<?php

namespace App\Domain\Secretaries\Plans\Actions;

use App\Domain\Secretaries\Plans\DTO\StorePlanDTO;
use App\Domain\Secretaries\Plans\DTO\UpdatePlanDTO;
use App\Domain\Secretaries\Plans\Models\Plan;
use Exception;
use Illuminate\Support\Facades\DB;

class UpdatePlanAction
{
    /**
     * @param UpdatePlanDTO $dto
     * @return Plan
     * @throws Exception
     */
    public function execute(UpdatePlanDTO $dto): Plan
    {
        DB::beginTransaction();
        try {
            $plan = $dto->getPlan();
            $plan->responsible_id = $dto->getResponsibleId();
            $plan->reporter_id = $dto->getReporterId();
            $plan->title = $dto->getTitle();
            $plan->responsible = $dto->getResponsible();
            $plan->lifetime = $dto->getLifetime();
            $plan->reporter = $dto->getReporter();
            $plan->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();

        return $plan;
    }
}
