<?php

namespace App\Domain\Secretaries\Issues\DTO;

use App\Domain\Secretaries\Issues\Models\Issue;

class UpdateIssueDTO
{
    /**
     * @var int
     */
    private int $direction_id;

    /**
     * @var int
     */
    private int $user_id;

    /**
     * @var int
     */
    private int $agenda_id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var Issue
     */
    private Issue $issue;

    /**
     * @param array $data
     * @return UpdateIssueDTO
     */
    public static function fromArray(array $data): UpdateIssueDTO
    {
        $dto = new self();
        $dto->setDirectionId($data['direction_id']);
        $dto->setTitle($data['title']);
        $dto->setIssue($data['issue']);
        $dto->setAgendaId($data['agenda_id']);
        $dto->setUserId($data['user_id']);

        return $dto;
    }

    /**
     * @return int
     */
    public function getDirectionId(): int
    {
        return $this->direction_id;
    }

    /**
     * @param int $direction_id
     */
    public function setDirectionId(int $direction_id): void
    {
        $this->direction_id = $direction_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Issue
     */
    public function getIssue(): Issue
    {
        return $this->issue;
    }

    /**
     * @param Issue $issue
     */
    public function setIssue(Issue $issue): void
    {
        $this->issue = $issue;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getAgendaId(): int
    {
        return $this->agenda_id;
    }

    /**
     * @param int $agenda_id
     */
    public function setAgendaId(int $agenda_id): void
    {
        $this->agenda_id = $agenda_id;
    }
}
