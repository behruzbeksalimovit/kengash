<?php

namespace App\Domain\Secretaries\Issues\DTO;

class StoreIssueDTO
{
    /**
     * @var int
     */
    private int $direction_id;

    /**
     * @var int
     */
    private int $user_id;

    /**
     * @var int
     */
    private int $agenda_id;

    /**
     * @var array
     */
    private array $title;

    /**
     * @param array $data
     * @return StoreIssueDTO
     */
    public static function fromArray(array $data): StoreIssueDTO
    {
        $dto = new self();
        $dto->setDirectionId($data['direction_id']);
        $dto->setTitle($data['title']);
        $dto->setAgendaId($data['agenda_id']);
        $dto->setUserId($data['user_id']);

        return $dto;
    }

    /**
     * @return int
     */
    public function getDirectionId(): int
    {
        return $this->direction_id;
    }

    /**
     * @param int $direction_id
     */
    public function setDirectionId(int $direction_id): void
    {
        $this->direction_id = $direction_id;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @param array $title
     */
    public function setTitle(array $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getAgendaId(): int
    {
        return $this->agenda_id;
    }

    /**
     * @param int $agenda_id
     */
    public function setAgendaId(int $agenda_id): void
    {
        $this->agenda_id = $agenda_id;
    }
}
