<?php

namespace App\Domain\Secretaries\Issues\Models;

use App\Domain\Members\Infos\Models\Info;
use App\Domain\Secretaries\Agendas\Models\Agenda;
use App\Domain\Secretaries\Directions\Models\Direction;
use App\Domain\Secretaries\Members\Models\Member;
use App\Domain\Secretaries\Plans\Models\Plan;
use App\Domain\Votes\Models\Vote;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Issue extends Model
{
    use HasFactory;

    /**
     * @var int
     */
    protected $perPage = 15;

    protected $with = ['member'];

    /**
     * @return BelongsTo
     */
    public function direction(): BelongsTo
    {
        return $this->belongsTo(Direction::class);
    }

    public function infos()
    {
        return $this->hasMany(Info::class);
    }

    public function agenda()
    {
        return $this->hasMany(Agenda::class,'id','agenda_id');
    }

    public function member()
    {
        return $this->hasMany(Member::class,'user_id','user_id');
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }
}
