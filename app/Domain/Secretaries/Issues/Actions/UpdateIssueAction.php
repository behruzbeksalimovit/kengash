<?php

namespace App\Domain\Secretaries\Issues\Actions;

use App\Domain\Secretaries\Issues\DTO\StoreIssueDTO;
use App\Domain\Secretaries\Issues\DTO\UpdateIssueDTO;
use App\Domain\Secretaries\Issues\Models\Issue;
use Exception;
use Illuminate\Support\Facades\DB;

class UpdateIssueAction
{
    /**
     * @param UpdateIssueDTO $dto
     * @return Issue
     * @throws Exception
     */
    public function execute(UpdateIssueDTO $dto): Issue
    {
        DB::beginTransaction();
        try {
            $issue = $dto->getIssue();
            $issue->direction_id = $dto->getDirectionId();
            $issue->user_id = $dto->getUserId();
            $issue->agenda_id = $dto->getAgendaId();
            $issue->title = $dto->getTitle();
            $issue->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $issue;
    }
}
