<?php

namespace App\Domain\Secretaries\Issues\Actions;

use App\Domain\Secretaries\Issues\DTO\StoreIssueDTO;
use App\Domain\Secretaries\Issues\Models\Issue;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreIssueAction
{
    /**
     * @param StoreIssueDTO $dto
     * @return array
     * @throws Exception
     */
    public function execute(StoreIssueDTO $dto): array
    {
        $data = array();
        DB::beginTransaction();
        try {
            for ($i=0; $i<count($dto->getTitle()); $i++)
            {
                $issue = new Issue();
                $issue->direction_id = $dto->getDirectionId();
                $issue->user_id = $dto->getUserId();
                $issue->agenda_id = $dto->getAgendaId();
                $issue->title = $dto->getTitle()[$i];
                $issue->save();
                $data[$i] = $issue;
            }
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $data;
    }
}
