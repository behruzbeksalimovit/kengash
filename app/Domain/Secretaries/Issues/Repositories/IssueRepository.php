<?php

namespace App\Domain\Secretaries\Issues\Repositories;

use App\Domain\Secretaries\Issues\Models\Issue;
use App\Domain\Traits\LoginTrait;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class IssueRepository
{
    use LoginTrait;
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return Issue::query()
            ->with(['direction','agenda','member'])
            ->orderBy('id', 'desc')
            ->get()
            ->groupBy('direction.title')
            ->paginate(15);
    }

    /**
     * @return Builder[]|Collection
     */
    public function getAll(): Collection|array
    {
        return Issue::query()
            ->with(['direction','agenda','member'])
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @param $request
     * @return LengthAwarePaginator
     * @throws GuzzleException
     */
    public function getMemberIssue($request): LengthAwarePaginator
    {
        $result = $this->login($request);
        return Issue::query()
            ->with(['direction','agenda','member'])
            ->where('user_id', '=', $result->id)
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    /**
     * @param $direction_id
     * @return LengthAwarePaginator
     */
    public function getDirectionIssue($direction_id): LengthAwarePaginator
    {
        return Issue::query()
            ->with(['direction','direction.issues.infos','agenda','member'])
            ->where('direction_id', '=', $direction_id)
            ->orderBy('id', 'desc')
            ->paginate(8);
    }

    /**
     * @param $agenda_id
     * @param $user_id
     * @return LengthAwarePaginator
     */
    public function getIssueRector($agenda_id,$user_id): LengthAwarePaginator
    {
        return Issue::query()
            ->with(['direction','direction.issues.infos','agenda','member'])
            ->where('agenda_id', '=', $agenda_id)
            ->where('user_id', '=', $user_id)
            ->orderBy('id', 'desc')
            ->paginate(20);
    }

    public function getAgendaIssue($agenda_id): LengthAwarePaginator
    {
        return Issue::query()
            ->with(['direction','direction.issues.infos','agenda','member','votes'])
            ->where('agenda_id', '=', $agenda_id)
            ->orderBy('id', 'desc')
            ->get()
            ->groupBy('direction.title')
            ->paginate(8);
    }
}
