<?php

namespace App\Models;

use App\Domain\Secretaries\Directions\Models\Direction;
use App\Domain\Secretaries\Issues\Models\Issue;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $with = ['direction','issue'];

    public function direction()
    {
        return $this->belongsTo(Direction::class);
    }

    public function issue()
    {
        return $this->belongsTo(Issue::class);
    }
}
