<?php

use App\Http\Controllers\Attendances\AttendanceController;
use App\Http\Controllers\Clients\ClientController;
use App\Http\Controllers\Departments\DepartmentController;
use App\Http\Controllers\Histories\HistoryController;
use App\Http\Controllers\Logins\LoginController;
use App\Http\Controllers\Members\Infos\InfoController;
use App\Http\Controllers\Questions\QuestionController;
use App\Http\Controllers\Secretaries\Agendas\AgendaController;
use App\Http\Controllers\Secretaries\Directions\DirectionController;
use App\Http\Controllers\Secretaries\Issues\IssueController;
use App\Http\Controllers\Secretaries\Members\MemberController;
use App\Http\Controllers\Secretaries\Plans\PlanController;
use App\Http\Controllers\Votes\VoteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//LOGIN
Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', [LoginController::class, 'logout']);
//END LOGIN

Route::get('member',[MemberController::class,'getAll']);
Route::get('agenda/all', [AgendaController::class, 'getAll']);
Route::apiResource('histories', HistoryController::class);
Route::apiResource('questions', QuestionController::class);
Route::get('history/all', [HistoryController::class,'getAll']);
Route::get('question/all', [QuestionController::class,'getAll']);
Route::get('/image/{user_id}', [\App\Http\Controllers\Rectors\RectorController::class,'index']);

//KOTIB BO'LIMI START
Route::group(['prefix' => 'secretaries', 'middleware' => 'secretary'], function () {
    Route::apiResource('plans', PlanController::class);
    Route::apiResource('agendas', AgendaController::class);
    Route::apiResource('directions', DirectionController::class);
    Route::apiResource('issues', IssueController::class);
    Route::apiResource('members', MemberController::class);
    Route::get('direction/paginate', [DirectionController::class, 'getPaginateHomePage']);
    Route::get('direction/all', [DirectionController::class, 'getAll']);
    Route::post('departments',[DepartmentController::class,'departments']);
    Route::post('departments/{id}',[DepartmentController::class,'departmentSub']);
    Route::get('info/all', [InfoController::class, 'getAll']);
    Route::get('info/{issue_id}/agenda',[InfoController::class,'getMemberAgenda']);
    Route::get('issue/all', [IssueController::class, 'getAll']);
    Route::get('direction/{direction_id}/issue', [IssueController::class, 'getDirectionIssue']);
    Route::get('member/all', [MemberController::class, 'getAll']);
    Route::post('member/{member_id}/status', [MemberController::class, 'updateStatus']);
    Route::get('plan/all', [PlanController::class, 'getAll']);
    Route::get('agenda/paginate', [AgendaController::class, 'getPaginateHomePage']);
    Route::get('agenda/all', [AgendaController::class, 'getAll']);
});
//KOTIB BO'LIMI END


//A'ZOLAR BO'LIMI START
Route::group(['prefix' => 'members', 'middleware' => 'member'], function () {
    Route::apiResource('infos', InfoController::class);
    Route::get('info/issue/{issue_id}', [InfoController::class,'getIssueInfo']);
    Route::get('agenda', [AgendaController::class, 'getPaginateHomePage']);
    Route::get('issue', [IssueController::class, 'getMemberIssue']);
    Route::post('attendance', [AttendanceController::class, 'store']);
    Route::post('/vote',[VoteController::class,'store']);
});
//A'ZOLAR BO'LIMI END


//REKTOR BO'LIMI START
Route::group(['prefix' => 'rectors', 'middleware' => 'rector'], function () {
    Route::get('/', function (){
        return 'rector';
    });
    Route::get('/agenda',[AgendaController::class,'getRectorAgenda']);
    Route::get('/direction',[DirectionController::class,'getRectorDirection']);
    Route::get('/issue',[IssueController::class,'getRectorIssue']);
    Route::get('/agenda/{agenda_id}/user/{user_id}/issue',[IssueController::class,'getIssueRector']);
    Route::get('/agenda/{agenda_id}/issue',[IssueController::class,'getAgendaIssue']);
    Route::get('/member',[MemberController::class,'getRectorMember']);
    Route::get('/info',[InfoController::class,'getAll']);
    Route::get('/issue/{issue_id}/user/{user_id}/info',[InfoController::class,'getMemberIssueRectorAll']);
    Route::get('attendance/count/{date}', [AttendanceController::class,'getAllAttendanceCount']);
    Route::get('attendance/all', [AttendanceController::class,'getAll']);
    Route::get('attendance/filtr/{date}', [AttendanceController::class,'getDateFiltr']);
    Route::get('vote/all', [VoteController::class,'getAll']);
    Route::get('vote/tarafdorlar/agenda/{agenda_id}', [VoteController::class,'tarafdorlar']);
    Route::get('vote/betaraflar/agenda/{agenda_id}', [VoteController::class,'betaraflar']);
    Route::get('vote/qarshilar/agenda/{agenda_id}', [VoteController::class,'qarshilar']);
    Route::get('vote/ovozbermaganlar/agenda/{agenda_id}', [VoteController::class,'ovozbermaganlar']);
    Route::get('vote/jami/agenda/{agenda_id}', [VoteController::class,'jami']);
    Route::get('plan/all', [PlanController::class, 'index']);
    Route::get('vote/{issue_id}', [VoteController::class,'filtrIssue']);

    Route::get('vote/issue/{issue_id}/tarafdorlar', [VoteController::class,'getIssueVoteTarafdorlar']);
    Route::get('vote/issue/{issue_id}/betaraflar', [VoteController::class,'getIssueVoteBetaraflar']);
    Route::get('vote/issue/{issue_id}/qarshilar', [VoteController::class,'getIssueVoteQarshilar']);
    Route::get('vote/issue/{issue_id}/ovozbermaganlar', [VoteController::class,'getIssueVoteOvozBermaganlar']);
    Route::get('vote/issue/{issue_id}/jami', [VoteController::class,'getIssueVoteJami']);
    Route::get('question/issue/{issue_id}', [QuestionController::class,'getIssueQuestion']);
});
//REKTOR BO'LIMI END

Route::post('admin/store', [ClientController::class,'store']);
Route::get('client/get', [ClientController::class,'index']);
